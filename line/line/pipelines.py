# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import os
import requests


class LinePipeline(object):
    def process_item(self, item, spider):
        self._local(item)

    def _local(self, item):
        folder = os.path.abspath('../tmp/{name}/'.format_map(item))
        os.makedirs(folder, exist_ok=True)
        counter = 0
        for url in item['urls']:
            with open('{}/{}.png'.format(folder, counter), 'wb') as f:
                r = requests.get(url)
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
            counter += 1
