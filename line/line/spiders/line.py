import logging
import os
import re
from datetime import datetime
import scrapy


class Line(scrapy.Spider):
    name = 'line'

    def __init__(self, name, url):
        self.name = name
        self.start_url = url
        super().__init__()

    def start_requests(self):
        yield scrapy.Request(url=self.start_url, callback=self.parse)

    def parse(self, response):

        # Extract CSS
        urls = response.css('span.mdCMN09Image').css('span[style]::attr(style)').extract()
        yield {
            'name': self.name,
            'urls': [re.search(r'\((.*?)\)', url).group(1).split(';')[0] for url in urls]
        }
